
class XOlogic{

  late List<List<bool?>> grid ;
  late bool isX ;

  XOlogic(){
    grid = [
      for(int i = 0 ; i < 3 ; i++)
      [null,null,null]
    ];
    isX = true;
  }

  void play(int i,int j){
    if(grid[i][j] == null) {
      grid[i][j] = isX;
      isX = !isX;
    }
  }

  String getPosition(int i , int j) {
    if(grid[i][j] == null)
      return '';
    return grid[i][j]! ? 'X' : 'O';
  }

  String? check(){
    return 'X win';
  }

}