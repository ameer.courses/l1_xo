import 'package:flutter/material.dart';
import 'package:xo/logic.dart';

XOlogic logic = XOlogic();

class XObody extends StatefulWidget {
  const XObody({Key? key}) : super(key: key);

  @override
  State<XObody> createState() => _XObodyState();
}

class _XObodyState extends State<XObody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [Colors.black54,Colors.black87,],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight
        )
      ),
      child: Column(
        children: [
          for(int i = 0 ; i < 3 ; i++)
            XOrow(i),
          SizedBox(height: 25,),
          Replay(
              (){
                setState((){
                  logic = XOlogic();
                });
              }
          ),
          SizedBox(height: 25,),
        ],
      ),
    );
  }
}

class XOrow extends StatelessWidget {

  final int i;

  XOrow(this.i);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        children: [
          for(int j = 0 ; j < 3 ; j++)
            XOwidget(i,j),
        ],
      ),
    );
  }
}

class XOwidget extends StatefulWidget {
  final int i;
  final int j;

  XOwidget(this.i, this.j);

  @override
  State<XOwidget> createState() => _XOwidgetState();
}

class _XOwidgetState extends State<XOwidget> {

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        child: Container(
          width: double.infinity,
          height: double.infinity,
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
            border: Border.all(
                color: Colors.white,
                width: 5,
            ),
            borderRadius: BorderRadius.circular(25)
          ),
          child: Text(logic.getPosition(widget.i, widget.j),style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w700,
            fontSize: 25
          ),),
          alignment: Alignment.center,
        ),
        onTap: (){
            setState((){
              logic.play(widget.i, widget.j);
            });
            String? result = logic.check();
            if(result != null)
              showDialog(
                  context: context,
                  builder: (context) =>
                      Dialog(
                        child: Text(result),
                      )
              );
        },
      ),
    );
  }

}

class Replay extends StatelessWidget {

  final GestureTapCallback onTap;
  Replay(this.onTap);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: 120,
        height: 60,
        alignment: Alignment.center,
        child: Text('replay',
        style: TextStyle(
          fontSize: 20
        ),
        ),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(25)
        ),
      ),
      onTap: onTap
    );
  }
}

